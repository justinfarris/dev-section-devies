# Dev Section Devies

## Info

This repo contains the files necessary to print your own Dev Section Devie, along with this readme that walks you through the process. Links and instructions are below but please feel free to contribute if there are ways to improve the process. 

## Supplies and Software

#### 3D Printer
The instructions, videos and GCODE files are produced with/for the Prusa MK3 printer. It's a great printer (open sourced!) but any printer can be used here following a similar process. The only main requirement you'll need is the ability to change colors mid print, along with slicing software that enables you to set the switching layer. Beyond that any printer should do. 

#### Filament
It's recommended to use PLA for this model, it's produces smooth prints and is fairly easy to work with. It also seems to have the most availability of colors that are affordable. Filament recommendations are below: 

**Orange** - https://www.amazon.com/gp/product/B00J0EE1D4/
**Purple** - https://www.amazon.com/gp/product/B01EKEMUIG/

#### SketchUp
To make the nameplate you'll need some kind of 3D modeling software. SketchUp is the recommended choice since it's easy to use and in most scenarios free. For the majority of the changes you'll need to make the web based version should be suitable, especially for nameplate creation. If you need to make any heavy modifications to the model you may need the desktop software. Links to both are below:

SketchUp Web: https://app.sketchup.com/app
SketchUp Downloads: https://www.sketchup.com/download/all

*note: all models in the repo are saved to be compatible to the 2017 "Make" edition of SketchUp. This version is free, but doesn't support exporting to an STL file. You can do that with the web application, or purchase a later version and update the file.*

#### Slicing Software
PrusaSlicer is used in the videos and used to create the gcode files. This slicer 'ships with' the Prusa 3D printers but can be used for other printer models as well. 

https://www.prusa3d.com/prusaslicer/

#### Glue
You're going to need some CA (Cyanoacrylate) glue to glue the parts together. 

https://www.amazon.com/Insta-Cure-Filling-Bob-Smith-Ind/dp/B0000DD1QQ/

## Instructions

*TThese instructions assumes you only need to modify the nameplate, and print the existing GCODE files as stored in the repo.*

#### Print the base and top
1. **Print the base.** select devie-bottom-PLA.gcode and print that file using purple filament.
1. **Print the top.** select devie-top-PLA.gcode and begin the print with purple filament. After the purple portion is complete your printer will alert you to swap the filament. Remove the purple, and replace with orange to complete the print. 

#### Create the nameplate
1. Using [SketchUp](https://app.sketchup.com/app) open the nameplate.skp file
1. Remove the existing name by selecting that part of the object and hitting the delete key
1. Use the "3D Text" tool under "Rectangles" and enter the FIRST name of the winner
1. Ensure the following text settings are applied - Font: Open Sans, Text Height - 8mm, Extrusion Height - 2mm, Text Filled and Text Extruded should be selected.
1. Use the "3D Text" tool under "Rectangles" and enter the LAST name of the winner
1. Make sure the same text settings are applied
1. Adjust the positioning of the text on the nameplate with the move tool
1. Export the model as an STL under File > Export > STL

Video: https://youtu.be/2DSt2pIM1_U

#### Prepare and print the nameplate
1. Using the PrusaSlicer software drag and drop your .STL file onto the print bed
1. Ensure the print is oriented "back" down so the words are facing upward
1. Using .15 MM layer height slice the model
1. Set the color change layer to the first layer of the lettering
1. Export GCODE and print

Video: https://youtu.be/Y17bh2aQhIU

#### Glue it all together

1. Glue the nameplate to the base and let dry
1. Glue the top Tanuki piece to the base and let dry


## Additional Info

* Unfiltered Playlist: https://www.youtube.com/playlist?list=PL05JrBw4t0KpqGSSn7wD49D5L_t11vU7J



